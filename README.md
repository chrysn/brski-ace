# Provisioning ACE credentials through BRSKI

This is the working area for the individual Internet-Draft, "Provisioning ACE credentials through BRSKI".

* [Datatracker Page](https://datatracker.ietf.org/doc/draft-amsuess-ace-brski-ace)
* [Individual Draft](https://datatracker.ietf.org/doc/html/draft-amsuess-ace-brski-ace)


## Contributing

See the
[guidelines for contributions](https://gitlab.com/chrysn/brski-ace/-/blob/main/CONTRIBUTING.md).

Contributions can be made by creating pull requests.
The GitLab interface supports creating pull requests using the Edit (✏) button.


## Command Line Usage

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

Command line usage requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/main/doc/SETUP.md).

