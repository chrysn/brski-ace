LIBDIR := lib
include $(LIBDIR)/main.mk

$(LIBDIR)/main.mk:
ifneq (,$(shell grep "path *= *$(LIBDIR)" .gitmodules 2>/dev/null))
	git submodule sync
	git submodule update $(CLONE_ARGS) --init
else
	git clone -q --depth 10 $(CLONE_ARGS) \
	    -b main https://github.com/martinthomson/i-d-template $(LIBDIR)
endif

%.validated.json: %.txt
	CACHE="$$(curl https://yangcatalog.org/yangvalidator/v2/upload-files-setup -X POST --data '{"latest":true}' | jq -r .output.cache)"; \
	echo "Set up cache for $@: CACHE=$${CACHE}"; \
	curl https://yangcatalog.org/yangvalidator/v2/draft-validator/"$${CACHE}" -X POST -F data=@"$<" > $@

check-yang: $(patsubst %.md,%.validated.json,${drafts_source})
	# Using system shell b/c otherwise we'd have to add termcolor to the lib set up
	# venv (which for portability wouldn't be too bad)
	/usr/bin/python3 json2pretty.py $^

 
.PHONY: check-yang
