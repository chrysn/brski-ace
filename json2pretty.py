import json
import sys
from termcolor import cprint
files = sys.argv[1:]
for f in files:
    print(f)
    print()
    output = json.load(open(f))['output']
    for (yangfile, outputs) in output.items():
        # weirdly special key
        if yangfile == "xym":
            if outputs['stderr']:
                cprint("Warning: XYM had stderr", "red")
                print(output['stderr'])
            continue
        cprint(f"For YANG module named {yangfile}", "blue")
        for (tool, results) in outputs.items():
            print()
            print("Tool", end=" ")
            cprint(tool, "red" if results['code'] != 0 or results['stderr'] else "green")
            print(results['stdout'].strip())
            cprint(results['stderr'].strip(), "red")
