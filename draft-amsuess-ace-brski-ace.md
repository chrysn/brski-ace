---
v: 3

title: "Provisioning ACE credentials through BRSKI"
abbrev: "BRSKI for ACE"
docname: draft-amsuess-ace-brski-ace-latest
category: std

ipr: trust200902
workgroup: ace
keyword:
  - ace
  - brski
  - est
  - onboarding

stand_alone: yes
smart_quotes: no
pi: [toc, sortrefs, symrefs]

author:
 -
    name: Christian Amsüss
    country: Austria
    email: christian@amsuess.com

normative:
  8366bis: I-D.ietf-anima-rfc8366bis
  CWT: RFC8392
  ACE: RFC9200

informative:
  authz: I-D.selander-lake-authz
  ace-oscore: I-D.ietf-ace-oscore-profile


--- abstract

The autonomous onboarding mechanisms defined in ANIMA's voucher artifact
and the BRSKI protocol
provide a means of onboarding a device (the pledge) onto a PKI managed domain.
This document extends the voucher with expressions for onboarding it into a domain managed through ACE.

--- middle

# Introduction

\[ See abstract. \]

The main application pattern considered for this kind of enrollment is {{authz}}:
Following that document's pattern,
after basic networking services (possibly link-local when later used in combination with CoJP as in {{Appendix A of authz}}) are available,
the pledge initiates EDHOC to the lake-authz.arpa anycast address,
sending an encrypted identifier for its MASA (party W) in EAD_1.

This document adds the mechanisms by which,
in the course of this exchange,
the device is enrolled onto an {{ACE}} Authorization Server.

## Applicability preconditions

While ACE in general does not constrain the type of tokens used,
or how the authorization space is split up,
using the extensions of this document introduces additional conditions:

* The key used to authenticate the token is a COSE key,
  and {{CWT}} are used as tokens.

  The alternative to this constraint is to declare this a blob of some key;
  what it is depends would be preconfigured in the RS.
  While this is the general approach of ACE,
  the author considers it unsuitable for this particular case where a concrete identifier is assigned
  and thus should have concrete semantics.
  Users of any other key format may use this document as scaffolding for declaring an own YANG leaf instead.

  While this does allow symmetric keys in theory
  (and they are used in ACE, for example in the {{ace-oscore}}) profile),
  they should not be used in BRSKI deployments,
  as the secret key would be shared with the MASA as it signs the voucher.
  \[ See also the Open Questions section. \]

* The pledge is identified with a single audience value.
  (More precisely, there is an "aud" claim conveyed in the CWTs that can be checked for equality against a configured value).

  This rules out setups in which multiple security systems coinhabit the pledge,
  are enrolled in a single step to a single AS,
  but act independently at runtime.

  Future iterations of this document may relax this,
  but will always need to express a condition based on which the pledge will know whether or not it may act on a token.

Using these extensions introduces no constraints on the type of scope values used with tokens.
The structure of scope values needs to be agreed between the AS and the RS out of band.
Typically, the AS will have configured knowledge of how to generate scope values that match the hard-coded model of the RS's firmware
from authorizations of its native model.

# Voucher extensions

This specification adds two leaf nodes to the voucher artifact
defined in {{Section 6.1 of 8366bis}}
\[ this is currently done in a monkey-sees-monkey-does fashion,
rather than having the yang files standalone and using pyang to build the tree as is done in 8366bis \]:

~~~
module: ietf-voucher
  augment voucher:
    +-- ace-as-key?                      binary
    +-- ace-aud?                         string
    +-- ace-as-uri?                      string
~~~

## YANG Module

~~~
<CODE BEGINS> file "ietf-ace-brski-ace@2023-07-07.yang"
{::include ietf-ace-brski-ace.yang}
<CODE ENDS>
```

## Examples

The example of {{Section 6.2 of 8366bis}},
when used to enroll in at an ACE AS instead of a PKI domain,
looks like this:

```
{
  "ietf-voucher:voucher": {
    "created-on": "2023-07-07T07:33:20Z",
    "assertion": "logged",
    "serial-number": "JADA123456789",
    "nonce": "base64encodedvalue==",
    "ace-as-pubk": "base64encodedpubkey=",
    "ace-aud": "jada89",
    "ace-as-uri": "https://as.registrar.example.net/token"
  }
}
~~~

A device accepting this voucher will accept tokens signed with the credials expressed as a COSE key in the ace-as-pubk field,
provided they are issued with an audience value of "jada89".

# Security Considerations

\[ TBD; in particular the open question on symmetric keys. \]

# IANA Considerations

\[ TBD: Request this for the YANG Module Names Registry;
the module itself is registered differently. \]

# Open questions

* There are probably missing steps in this specification;
  the current draft's intention is primarily to start discussion.

* Is there a shorter route to the same result I missed (and should take instead)?

  Is there a longer route (doing EST style onboarding into a PKI, and then obtain AS data by using those certificates) that I missed (and could reference and learn from)?

* Is there existing YANG terminology for ACE (or just OAuth) to use?

  There was some OAuth in the YANG of draft-kwatsen-netconf-http-client-server-03,
  but that was just FIXMEs, and later removed.

* AIU the voucher artifact data assembled by the registrar travels to the MASA to be signed during BRSKI.
  That's fine when we're shipping a pinned-domain-cert,
  and also when we're shipping as-public-key and an audience identifier (for the ACE EDHOC profile),
  but not when shipping a shared key (for the ACE OSCORE profile).

* Is this suitable use of BRSKI and the voucher?

* This document currently only describes expressing the ACE details in YANG for an ACE voucher.

  For use with more EST-like enrollments,
  it could define resources equivalent to the rt=ace.est.sen resources.

  Alternatively, such setups could use COMI to manipulate the AS.
  (But in the EST direction, would this need "pull mode COMI"?)

--- back

# Acknowledgments
{:numbered="false"}

TODO acknowledge.
